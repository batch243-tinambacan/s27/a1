// Users
{
    "id": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@mail.com",
    "password": "john1234",
    "isAdmin": false,
    "mobileNo": "09237593671"
    "orders" : [{
        "productID" : 25,
        "transactionDate": "08-15-2021",
        "status": "paid",
        "total": 1500
        }
    ]
}


// Orders
{
    "id": 10,
    "userId": 1,
    "productID" : 25,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 1500
}



// Products
{
    "id": 25,
    "name": "Humidifier",
    "description": "Make your home smell fresh any time.",
    "price": 300,
    "stocks": 1286,
    "isActive": true,
    "orders" : [{
        "userID" : 25,
        "transactionDate": "08-15-2021",
        "status": "paid",
        "total": 1500
        }
    ]
}



// Another User
{
    "id": 2,
    "firstName": "Taylor",
    "lastName": "Swift",
    "email": "swifty@mail.com",
    "password": "swift1234",
    "isAdmin": false,
    "mobileNo": "09123593671"
    "orders" : [{
        "productID" : 13,
        "transactionDate": "12-13-1989",
        "status": "paid",
        "total": 1300
        }
    ]
}


// Another Order
{
    "id": 11,
    "userId": 2,
    "productID" : 13,
    "transactionDate": "12-13-1989",
    "status": "paid",
    "total": 1300
}


// Another Product
{
    "id": 13,
    "name": "sweater",
    "description": "Make yourself warm any time.",
    "price": 13000,
    "stocks": 1989,
    "isActive": true,
    "orders" : [{
        "userID" : 13,
        "transactionDate": "12-13-1989",
        "status": "paid",
        "total": 1300
        }
    ]
}